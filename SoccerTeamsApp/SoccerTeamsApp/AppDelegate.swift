//
//  AppDelegate.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let container:Container = Container()
       

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor =  UIColor(red: 31.0/255, green: 56.0/255, blue: 69.0/255, alpha: 0)
        UINavigationBar.appearance().isTranslucent = false
        configureServices()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate{
    func configureServices()->Void{
        container.register(TeamsServiceProtocol.self){
            _ in
            let teamService = TeamService()
            return teamService
        }
        
        container.register(TeamListPresenterProtocol.self){
            r in
            let teamService = r.resolve(TeamsServiceProtocol.self)
            let presenter = TeamListPresenter(service: teamService!)
            return presenter
        }
        
        container.register(DetailTeamPresenterProtocol.self){
            r in
            let teamService = r.resolve(TeamsServiceProtocol.self)
            let detailTeamPresenter = DetailTeamPresenter(service: teamService!)
            return detailTeamPresenter
        }
        
        container.register(ListOfTeamsViewController.self){
            r in
            let controller = ListOfTeamsViewController()
            controller.teamListPresenter = r.resolve(TeamListPresenterProtocol.self)
            return controller
        }
        container.register(DetailTeamViewController.self){
            r in
            let controller = DetailTeamViewController()
            controller.detailTeamPresenter = r.resolve(DetailTeamPresenterProtocol.self)
            return controller
        }
        
}

}

