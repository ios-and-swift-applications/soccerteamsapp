//
//  ImageService.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit


class ImageService  {
    
    private static let cache = NSCache<NSString, UIImage>()
    
    // funcion that have 2 parameters and is used for get a image from internet or from memory cache
    static func getImage(urlImage:String,completion:@escaping(UIImage)-> Void){
        guard let imageUrl = URL(string: urlImage) else{
            return
        }
        var imageFound : UIImage?
        imageFound = cache.object(forKey: imageUrl.absoluteString as NSString)
        
        if imageFound != nil{
            completion(imageFound!)
        }else{
            downloadImage(urlImage: imageUrl,completion: completion)
        }
    }
    // get image from url, doing a called by urlSesion and convert the data to Data object, also if the image dont exist in cache it is saved in cache
    static func downloadImage(urlImage:URL,completion:@escaping(UIImage)->Void){
        
        let downladImageTask = URLSession.shared.dataTask(with: urlImage, completionHandler: {data,urlResult,error in
            var imageFound : UIImage?
            if let dataImage = data{
                imageFound = UIImage(data: dataImage)
            }
            
            if imageFound != nil{
                self.cache.setObject(imageFound!, forKey: urlImage.absoluteString as NSString)
            }
            DispatchQueue.main.async {
                completion(imageFound!)
            }
            
        })
        downladImageTask.resume()
    }
}
