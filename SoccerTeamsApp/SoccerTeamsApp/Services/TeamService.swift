//
//  TeamServices.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON

class TeamService : TeamsServiceProtocol {
    
    var header : HTTPHeaders = ["Content-type":"application/josn","Accept":"application/josn"]
    
    // Function for get alls teamd from a league in especify
    func getAllTeams(leagueName:String,completion:@escaping([TeamCell])->Void){
        var listTeams : [TeamCell] = [TeamCell]()
        Alamofire.request(APIConstans.URL_API_LIST_TEAMS+leagueName, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseJSON{
            response in
            switch response.result{
            case .success(let data):
                guard let dataResult = response.data else{
                    return
                }
                do{
                    let json = try JSON(data:dataResult)
                    for (_, subJson) in json["teams"] {
                        guard let teamResult = self.getValuedFromJson(subJson: subJson) else{
                            return
                        }
                        listTeams.append(teamResult)
                    }
                }catch{
                    
                }
                
                DispatchQueue.main.async {
                    completion(listTeams)
                }
            case .failure(let error):
                print("ocurrio un error \(error)")
            }
        }
        
    }
    // function that do a loop in the Json response and get properties that only are necesary for populate tha list pf temas, in this case it return a TeamCell object
    private func getValuedFromJson(subJson:JSON)->TeamCell?{
        var team : TeamCell?
        if  let idTeamValue = subJson["idTeam"].string,
            let nameTeamValue = subJson["strTeam"].string,
            let strTeamBadgeValue = subJson["strTeamBadge"].string,
            let strStadiumValue = subJson["strStadium"].string{
            team = TeamCell(idTeam:idTeamValue,strTeam:nameTeamValue,strTeamBadge:strTeamBadgeValue, strStadium:strStadiumValue)
        }
        return team
    }
    
    // function that get a tema by your ID, doing a http resquets at the API
    func getTeamById(idTeam:String,completion:@escaping(TeamDetail)-> Void){
        var detailTeam : TeamDetail = TeamDetail()
        let urlEndPoint = APIConstans.URL_API_DEATIL_TEAM + idTeam
        Alamofire.request(urlEndPoint, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseJSON{ response in
            switch response.result{
            case .success(let data):
                guard let dataResult = response.data else{
                    return
                }
                do{
                    let json = try JSON(data:dataResult)
                    for (_, subJson) in json["teams"] {
                        guard let teamDetailResult = self.getValuesForDetailTeam(subJson: subJson) else{
                            return
                        }
                        detailTeam = teamDetailResult
                    }
                    DispatchQueue.main.async {
                        completion(detailTeam)
                    }
                }catch{
                    
                }
            case .failure(let error):
                print("error \(error)")
            }
        }
    }
    
    // function that get the values for the properties that are necesary en the view of detail of the team.
    func getValuesForDetailTeam(subJson:JSON) -> TeamDetail? {
        var Detailteam : TeamDetail?
        var idTeam:String?
        var strTeam:String?
        var strDescriptionEN:String?
        var intFormedYear:String?
        var strTeamBadge:String?
        var strTeamJersey:String?
        var strStadium:String?
        var strWebsite:String?
        var strFacebook:String?
        var strTwitter:String?
        var strInstagram:String?
        if  let idTeamValue = subJson["idTeam"].string,
            let nameTeamValue = subJson["strTeam"].string,
            let strTeamBadgeValue = subJson["strTeamBadge"].string,
            let strStadiumValue = subJson["strStadium"].string,
            let strDescriptionENValue = subJson["strDescriptionEN"].string,
            let intFormedYearValue = subJson["intFormedYear"].string,
            let strTeamJerseyValue = subJson["strTeamJersey"].string,
            let strWebsiteValue = subJson["strWebsite"].string,
            let strTwitterValue = subJson["strTwitter"].string,
            let strInstagramValue = subJson["strInstagram"].string,
            let strFacebookValue = subJson["strFacebook"].string
        {
            Detailteam = TeamDetail(idTeam:idTeamValue,strTeam:nameTeamValue,strDescriptionEN:strDescriptionENValue,
                              intFormedYear:intFormedYearValue,strTeamBadge:strTeamBadgeValue,strTeamJersey:strTeamJerseyValue,strStadium:strStadiumValue,strWebsite:strWebsiteValue,strFacebook:strFacebookValue,strTwitter:strTwitterValue,strInstagram:strFacebookValue)
            
        }
        return Detailteam
    }
 
}
