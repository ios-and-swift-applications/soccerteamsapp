//
//  DetailTeamPresenter.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation


class DetailTeamPresenter: DetailTeamPresenterProtocol {
    
    var detailTeamView: DetailTeamViewProtocol?
    private var teamService : TeamsServiceProtocol?
    required init(service: TeamsServiceProtocol) {
      
        self.teamService = service
    }
    
    // function that does use of team service for get the detail information about a team by tyour id, and return the data at the view
    func getDeatilTeamById(idTeam: String) {
        self.detailTeamView?.starActivityIndicator()
        teamService?.getTeamById(idTeam: idTeam){[weak self] teamResult in
            if teamResult != nil{
                self?.detailTeamView?.stopActovityIndicator()
                self?.detailTeamView?.SetValuesToTeam(team: teamResult)
            }else{
                self?.detailTeamView?.showAlertError()
            }
        }
    }
    
    
    
}
