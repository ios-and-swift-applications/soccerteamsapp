//
//  TeamListPresenter.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class TeamListPresenter: TeamListPresenterProtocol {
    
    var listTeamView:TeamListViewProtocol?
    private var teamService:TeamsServiceProtocol!
    
    required init(service:TeamsServiceProtocol) {
        
        self.teamService = service
    }
    // fuctioj that invoke the teamservice y return tha array of teams for the view
    func getListOfTeams(leagueName:String) {
        self.listTeamView?.starActivityIndicator()
        self.teamService.getAllTeams(leagueName: leagueName){[weak self]
            listOfTeams in
            if listOfTeams.count > 0{
                self?.listTeamView?.stopActovityIndicator()
                self?.listTeamView?.reloadDataInTableView(listTeams: listOfTeams)
            }else {
                print("no trajo datos la solictud")
            }
            
        }
        
    }
    
    
    
    
    
}
