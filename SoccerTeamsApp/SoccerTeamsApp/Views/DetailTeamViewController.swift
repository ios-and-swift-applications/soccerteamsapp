//
//  DetailTeamViewController.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class DetailTeamViewController: UIViewController {
    
    @IBOutlet weak var InfoMainTeam: UIView!
    @IBOutlet weak var descritionTeamView: UIView!
    @IBOutlet weak var imgTeam: UIImageView!
    @IBOutlet weak var jerseyTeamView: UIView!
    @IBOutlet weak var nameTeam: UILabel!
   
    @IBOutlet weak var stadiumTeam: UILabel!
    
    @IBOutlet weak var socialNetworkView: UIView!
    @IBOutlet weak var yearOfFundationTeam: UILabel!
    
    @IBOutlet weak var descriptionTeam: UITextView!
    
    @IBOutlet weak var jerseyImageTeam: UIImageView!
    
    
    @IBOutlet weak var imgFacebook: UIImageView!
    
    @IBOutlet weak var imgTwitter: UIImageView!
    
    @IBOutlet weak var socialNetworkStackView: UIStackView!
    @IBOutlet weak var imgWebSite: UIImageView!
    @IBOutlet weak var imgInstagram: UIImageView!
    
    lazy var activiIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    lazy var activityIndicatorUtil : ActivityIndicatorUtil = ActivityIndicatorUtil()
    
    var urlFacePage:String = "";
    var urlTwitterPage : String = "";
    var urlInstagramPage :String = "";
    var urlWebSitePage:String = "";
    var detailTeamPresenter : DetailTeamPresenterProtocol?
    public var idTeam:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        self.detailTeamPresenter = appDelegate?.container.resolve(DetailTeamPresenterProtocol.self)
        self.detailTeamPresenter?.detailTeamView = self
        self.addGestureTapToImagesSocialNetwork()
        configurateRoundeImage()
        self.detailTeamPresenter?.getDeatilTeamById(idTeam: idTeam)
  
    }
    

}


// extension for to imoplement the DetailTeamViewProtocol
extension DetailTeamViewController : DetailTeamViewProtocol{
    func configurateRoundeImage() {
        imgTeam.clipsToBounds = true
        imgTeam.layer.cornerRadius = imgTeam.bounds.height / 2
    }
    
    func starActivityIndicator() {
        self.activiIndicator = activityIndicatorUtil.createActiviyIndicator(view: view)
        self.activityIndicatorUtil.showActivityIndicator()
        self.activiIndicator.startAnimating()
    }
    
    func stopActovityIndicator() {
           self.activityIndicatorUtil.removeActiviIndicatorView(view: view)
    }
    
    func SetValuesToTeam(team: TeamDetail) {
        nameTeam.text = team.strTeam
        stadiumTeam.text = team.strStadium
        yearOfFundationTeam.text = team.intFormedYear
        descriptionTeam.text = team.strDescriptionEN
        guard let urlImageTeam = team.strTeamBadge else {
            return
        }
        guard let urlImageJerseyTeam = team.strTeamJersey else {
            return
        }
        ImageService.getImage(urlImage: urlImageTeam){
            imageResult in
            if imageResult != nil{
                self.imgTeam.image = imageResult
            }
        }
        ImageService.getImage(urlImage: urlImageJerseyTeam){
            imageResult in
            if imageResult != nil{
                self.jerseyImageTeam.image = imageResult
            }
        }
        self.urlFacePage = team.strFacebook!
        self.urlTwitterPage = team.strTwitter!
        self.urlInstagramPage = team.strInstagram!
        self.urlWebSitePage = team.strWebsite!
        ValidUrlSocialNetWork()
    }
    
    func ValidUrlSocialNetWork(){
        if self.urlFacePage.isEmpty{
            imgFacebook.isHidden = true
        }
        if self.urlTwitterPage.isEmpty {
            imgTwitter.isHidden = true
        }
        if self.urlInstagramPage.isEmpty{
            imgInstagram.isHidden = true
        }
        if self.urlWebSitePage.isEmpty{
            imgWebSite.isHidden = true
        }
    }
    
    func goToSocialNetworkWebViewController(url:String){
        
        guard let socialNetworkVC = storyboard?.instantiateViewController(withIdentifier: "socialNetworkViewController") as? SocialNetWorksViewController else {
            return
        }
        socialNetworkVC.urlRequestPage = url
        navigationController?.pushViewController(socialNetworkVC, animated: true)
        
    }
    
    func addGestureTapToImagesSocialNetwork() {
        
        let tapFacebookImage = UITapGestureRecognizer(target: self, action: #selector(gotoFacebbokPage))
        let tapTwitterImage = UITapGestureRecognizer(target: self, action: #selector(gotoTwitterPage))
        let tapInstragramImage = UITapGestureRecognizer(target: self, action: #selector(gotoInstragramPage))
        let tapTWebSiteImage = UITapGestureRecognizer.init(target: self, action: #selector(gotoWebSitePge))
        
        imgFacebook.isUserInteractionEnabled = true
        imgTwitter.isUserInteractionEnabled = true
        imgInstagram.isUserInteractionEnabled = true
        imgWebSite.isUserInteractionEnabled = true
        imgFacebook.addGestureRecognizer(tapFacebookImage)
        imgTwitter.addGestureRecognizer(tapTwitterImage)
        imgInstagram.addGestureRecognizer(tapInstragramImage)
        imgWebSite.addGestureRecognizer(tapTWebSiteImage)
    }
    
    @objc private func gotoFacebbokPage(){
        goToSocialNetworkWebViewController(url: self.urlFacePage)
    }
    
    @objc private func gotoTwitterPage(){
        goToSocialNetworkWebViewController(url: self.urlTwitterPage)
    }
    
    @objc private func gotoInstragramPage(){
        goToSocialNetworkWebViewController(url: self.urlInstagramPage)
    }
    
    @objc private func gotoWebSitePge(){
        goToSocialNetworkWebViewController(url: self.urlWebSitePage)
    }
    
    func showAlertError() {
        let errorAlert = UIAlertController(title: "Error the load data", message: "A error ocurred loading the data", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        errorAlert.addAction(okAction)
        errorAlert.addAction(cancelAction)
        present(errorAlert,animated: true)
    }
}
