//
//  SocialNetWorksViewController.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit
import WebKit

class SocialNetWorksViewController: UIViewController {

    @IBOutlet weak var socialNetworkWKview: WKWebView!
    
    var urlRequestPage : String = ""
    lazy var webView : WKWebView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPage(url: urlRequestPage)
    }
    
    func loadPage(url:String){
        guard let urlrequest = URL(string: "https://"+url)else{
            return
        }
        let request = URLRequest(url: urlrequest)
        socialNetworkWKview.load(request)
        
    }
}
