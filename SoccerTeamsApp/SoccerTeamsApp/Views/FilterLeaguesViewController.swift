//
//  FilterLeaguesViewController.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/27/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class FilterLeaguesViewController: UIViewController {
    @IBOutlet weak var headerFilterView: UIView!
    
    @IBOutlet weak var BtnApplyButtonOutled: UIButton!
    @IBOutlet weak var FilterMainView: UIView!
    @IBOutlet weak var SegFilterLeagueControl: UISegmentedControl!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        configurateCornerRadiusButton(value:5)
        // Do any additional setup after loading the view.
    }
    @IBAction func btnApplyFilters(_ sender: UIButton) {
        
        let indexOpntion = SegFilterLeagueControl.selectedSegmentIndex
        switch indexOpntion {
        case 0:
            goToListTeamsVC(filterLeagueValue: APIConstans.ENGLISH_PREMIER_LEAGE)
        case 1:
            goToListTeamsVC(filterLeagueValue: APIConstans.ITALIAN_SERIE_A)
        default:
            goToListTeamsVC(filterLeagueValue: APIConstans.SPANISH_LEAGUE)
        }
    }
    
    
}

// extensions for to implement the fuctions of filterLeagueViewProtocol

extension FilterLeaguesViewController : filterLeagueViewProtocol{
    
    func goToListTeamsVC(filterLeagueValue:String){
        
        guard let listeamsVc = storyboard?.instantiateViewController(withIdentifier: "ListOfTeamsViewController") as? ListOfTeamsViewController else {
            return
        }
        listeamsVc.filterLeague = filterLeagueValue
        navigationController?.pushViewController(listeamsVc, animated: true)
    }
    func configurateCornerRadiusButton(value:CGFloat){
        BtnApplyButtonOutled.layer.masksToBounds = false
        BtnApplyButtonOutled.layer.cornerRadius = value
    }
}


