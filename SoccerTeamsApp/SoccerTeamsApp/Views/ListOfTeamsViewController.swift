//
//  ViewController.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ListOfTeamsViewController: UIViewController {
    
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var imgFilter: UIImageView!
    @IBOutlet weak var listTeamsView: UIView!
    
    @IBOutlet weak var listTeamsTableview: UITableView!
    var listTeams : [TeamCell] = [TeamCell]();
    var filterLeague:String = ""
    lazy var activityIndicator :UIActivityIndicatorView = UIActivityIndicatorView()
    lazy var activityUtil : ActivityIndicatorUtil = ActivityIndicatorUtil()
    var teamListPresenter : TeamListPresenterProtocol?
    var appDelegate : AppDelegate?
    override func viewDidLoad() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.teamListPresenter = self.appDelegate?.container.resolve(TeamListPresenterProtocol.self)
        self.teamListPresenter?.listTeamView = self
        configurateListTeamsTableView()
        AddTapGestureToImageFilter()
        getTeamsOfLeague()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    private func configurateListTeamsTableView(){
        self.listTeamsTableview.delegate = self
        self.listTeamsTableview.dataSource = self
    }
    
}
// extensions for the UItableView

extension ListOfTeamsViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTeams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let teamCell = listTeamsTableview.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as? TeamCellTableViewCell
        teamCell?.setTeam(team: listTeams[indexPath.row])
        return teamCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let idTeam = listTeams[indexPath.row].idTeam else {
            return
        }
        goToDetailTeam(idTeam: idTeam)
    }
}


// extensions for to implements the fuctions for protocol TeamListViewProtocol
extension ListOfTeamsViewController : TeamListViewProtocol{

    func getTeamsOfLeague() {
        
        if self.filterLeague.isEmpty{
            self.teamListPresenter?.getListOfTeams(leagueName: APIConstans.SPANISH_LEAGUE)
        }else{
            self.teamListPresenter?.getListOfTeams(leagueName: self.filterLeague)
        }
    }
    
    
    func reloadDataInTableView(listTeams:[TeamCell]) {
        self.listTeams = listTeams
        self.listTeamsTableview.reloadData()
    }
    
    func starActivityIndicator() {
        self.activityIndicator = activityUtil.createActiviyIndicator(view: view)
        self.activityUtil.showActivityIndicator()
        self.activityIndicator.startAnimating()
    }
    
    func stopActovityIndicator() {
        self.activityUtil.removeActiviIndicatorView(view: view)
    }
    func goToDetailTeam(idTeam: String) {
        
        guard let detailTeamVc = storyboard?.instantiateViewController(withIdentifier: "DetailTeamViewController") as? DetailTeamViewController else {
            return
        }
        detailTeamVc.idTeam = idTeam
        navigationController?.pushViewController(detailTeamVc, animated:    true)
    }
    
    func goToFilterViewController() {
        guard let filterVC = storyboard?.instantiateViewController(withIdentifier: "filtersViewController") as? FilterLeaguesViewController else {
            return
        }
        navigationController?.pushViewController(filterVC, animated: true)
    }
    
    func AddTapGestureToImageFilter() {
        imgFilter.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(goToFilterVc))
        imgFilter.addGestureRecognizer(tapGesture)
        
    }
    @objc private func  goToFilterVc(){
        self.goToFilterViewController()
    }
    
    
}

