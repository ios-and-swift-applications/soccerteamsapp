//
//  SoccerTeam.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation


struct TeamDetail {
    var idTeam:String?
    var strTeam:String?
    var strDescriptionEN:String?
    var intFormedYear:String?
    var strTeamBadge:String?
    var strTeamJersey:String?
    var strStadium:String?
    var strWebsite:String?
    var strFacebook:String?
    var strTwitter:String?
    var strInstagram:String?
    

    
}
