//
//  TeamCell.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import ObjectMapper


struct TeamCell{
    
    var idTeam:String?
    var strTeam:String?
    var strTeamBadge:String?
    var strStadium:String?

    
}
