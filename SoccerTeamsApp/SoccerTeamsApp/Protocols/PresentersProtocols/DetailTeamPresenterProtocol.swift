//
//  DetailListPresenterProtocol.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol DetailTeamPresenterProtocol {
    
    var detailTeamView : DetailTeamViewProtocol?{get set}
    init(service:TeamsServiceProtocol)
    
    func getDeatilTeamById(idTeam:String)->Void
    
    
    
}
