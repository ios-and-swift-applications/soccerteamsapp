//
//  FilterLeaguesViewProtocol.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/27/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit

protocol filterLeagueViewProtocol {
    func goToListTeamsVC(filterLeagueValue:String)
    func configurateCornerRadiusButton(value:CGFloat)
}
