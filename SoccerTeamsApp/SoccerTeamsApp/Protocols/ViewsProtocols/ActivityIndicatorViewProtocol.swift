//
//  ActivityIndicatorViewProtocol.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol ActivityIndicatorViewProtocol {
    
    func starActivityIndicator()->Void
    func stopActovityIndicator()->Void
    
    
}
