//
//  TeamListViewProtocol.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol TeamListViewProtocol : ActivityIndicatorViewProtocol {
    
    
    func reloadDataInTableView(listTeams:[TeamCell])->Void
    func goToDetailTeam(idTeam:String) -> Void
    func goToFilterViewController()-> Void
    func AddTapGestureToImageFilter()->Void
    func getTeamsOfLeague()->Void
}
