//
//  DetailTeamViewProtocol.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol DetailTeamViewProtocol : ActivityIndicatorViewProtocol{
    
    
    func configurateRoundeImage()-> Void
    func SetValuesToTeam(team:TeamDetail)-> Void
    func showAlertError()-> Void
    func addGestureTapToImagesSocialNetwork()->Void
    func goToSocialNetworkWebViewController(url:String)-> Void
    func ValidUrlSocialNetWork()->Void
}
