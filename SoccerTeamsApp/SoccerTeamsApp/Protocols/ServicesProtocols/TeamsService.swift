//
//  TeamsService.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 2/2/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol TeamsServiceProtocol {
    
    func getAllTeams(leagueName:String,completion:@escaping([TeamCell])->Void)
    func getTeamById(idTeam:String,completion:@escaping(TeamDetail)-> Void)
}
