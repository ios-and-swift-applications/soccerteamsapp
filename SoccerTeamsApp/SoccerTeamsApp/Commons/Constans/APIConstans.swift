//
//  ServiceConstans.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

struct APIConstans {
    
    static var URL_API_LIST_TEAMS :String = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l="
    static var URL_API_DEATIL_TEAM : String = "https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id="
    static var SPANISH_LEAGUE = "Spanish%20La%20Liga"
    static var ENGLISH_PREMIER_LEAGE :String = "English%20Premier%20League"
    static var ITALIAN_SERIE_A : String = "Italian%20Serie%20A"
}
