//
//  TeamCellTableViewCell.swift
//  SoccerTeamsApp
//
//  Created by Jorge luis Menco Jaraba on 1/26/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class TeamCellTableViewCell: UITableViewCell {

    
    @IBOutlet weak var contentTeamCell: UIView!
    
    @IBOutlet weak var teamView: UIView!
    
    @IBOutlet weak var teamBadged: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamStadium: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configRoundeImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTeam(team:TeamCell) -> Void {
        
        teamName.text = team.strTeam
        teamStadium.text = team.strStadium
        
        guard let badgedTeam = team.strTeamBadge else{
            return
        }
        ImageService.getImage(urlImage: badgedTeam){
            imageResult in
            if imageResult != nil {
                self.teamBadged.image = imageResult
            }
        }
    }
    
    func configRoundeImage(){
        teamName.layer.cornerRadius = teamName.bounds.height / 2
        teamName.clipsToBounds = true
    }
    
    

}

